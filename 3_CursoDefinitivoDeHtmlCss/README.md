## Etiquetas multimedia

### Tipos de imagenes

- Existen 2 tipos de imagenes Lossy(Con Perdida), LossLess(Sin perdida) - `LossLess`: Capturan todos los dartos de su archivo pero a la ves suelen ser muy pesadaos ya que estas no estarian perdieno la cantidad de colores nada del archivo original.

- `Lossy`: Estan se caracterizan por perder contenidose aproximan a la imagen originial, estas.

  ![Alt text](image.png)

#### Formatos de imagen para web

- GIF (Graphics Interchange Format): Formato de imagen sin pérdida, no se puede comprimir
- PNG 8 (Portable Network Graphics): Formato de imagen sin pérdida, uso de colores de 256, se utiliza para logotipos e iconos para la página.
- PNG 24 (Portable Network Graphics): Formato de imagen sin pérdida, utilización de colores ilimitados, alta calidad, si intentamos comprimir no ayudará demasiado por la gran cantidad de colores.
- JPG / JPEG (Photographic Experts Group): Formato de imagen con pérdida, perdemos calidad a la hora de comprimirlas, pero llegan a ser óptimas para la carga en la página web.
- SVG - Vector (Scalable Vector Graphics): Formato de imagen muy ligero sin pérdida, con svg no perdemos calidad, ya que está compuesta por vectores.
- WebP: Es un formato gráfico en forma de contenedor que sustenta tanto compresión con pérdida como sin ella. ​​Fue desarrollado por Google.

## Optimizacion de Imagenes:

- En promedio una pagina debe pesar 70KB
- Herramietntas para comprimir imagenes:
  - [Tiny PNG](https://tinypng.com/)
  - [Verefix](https://www.verexif.com/)

## Etiqueta figure

- Es una etiqueta contenedora para archivos multimedia imagenes

```
<figure>
    <img
        src="./pics/tinified/small.jpg"
        alt="Es una imagen de un perrito"
    />
    <figcaption>Es una imagen de un perrito</figcaption>
</figure>
```

- Semanticamente esta es correcat, ya que sino se tendria que usar `div` y un `p`

# CSS

## Pseudo clases y pseudo elementos

- Son celectores utilizados en css para aplicar estilos un elemento HTML en cierto estado o en una parte specifica de un elento.

  - Pseudo-clases: utilizado para aplicar stilos a elementos en ciertos `estados` especiales del elemento, se definenen utilizando el simbolo `:`
  - Estos son algunas pseudo-clases:

    - `:hover`: Se aplica cuando el cursor está sobre el elemento.
    - `:active`: Se aplica cuando el elemento está siendo presionado.
    - `:nth-child(n)`: Selecciona elementos que son el n-ésimo hijo de su padre.
    - `:focus`: Se aplica cuando el elemento recibe el enfoque.

  - Pseudo-elementos: se utilizan para seleccionar y estilizar partes `especificas` de un elemento que no son elementos reales en el `DOM`, su uso se define con los simbolos `::`, estos pseudo-elementos crean elementos ficticios permitiendo aplicar stilos a partes que no se pueden seleccionar directamente.
  - Estos son algunos pseudo-elementos:
    - `::before`: Crea un elemento ficticio `antes` del contenido del elemento seleccionado.
    - `::after`: Crea un elemento ficticio `después` del contenido del elemento seleccionado.
    - `::first-line`: Selecciona la primera línea de un elemento.
    - `::first-letter`: Selecciona la primera letra de un elemento.

- Diferencias

![Alt text](image-1.png)

## Herencia

- es el codigo css que se le va a pasar de un padre a su hijo la herencia se hace con la palabra `inherit`

```
# html
<Goku>
    <Goten>

    </Goten>
</Goku>
#css
Goku{
    cabello: negro;
}

Goten{
    cabello: inherit
}

```

## Especificidad en selectores

- para poder entender la herencia se debe saber de como se controla le orden al momento de declarar en CSS y el navegador tienen 3 puntos importantes en la especificidad de selectores:

1. Importancia: Si dos delcracioones tienene la misma importancia la especificidad nos ayua a que regla se va a aplicar, pero si las dos reglas tienen la misma especificidad es el orden el que decide el resultado final.
   - Primero cargan los estilos del navegador
   - declraciones normales de nnuestra hoja de estilos css
   - aplica los estilos finales que tiene el tag `!import`, esto es considerado una mala practica en css
2. Especificidad: Nos ayuda a verificar cuando se deve agregar satilos para un elelemnto si es por clase, id, o tag

   ![Alt text](image-2.png)

- algoritmo que sigue el navegador para implementar los estilos

![Alt text](image-3.png)

3. Orden de las fuentes: Esto hace referencia a que siempre se aplicarabn los estilos que s eencuetran alfinal de los stilos, puede sobreescribir a los elementos que se encuentran a inicio

## Combinadores: Adjacent Siblings (combinators)

![Alt text](image-4.png)

- combinador `<tag1> + <tag2>` indica a todo `<tag2>` que sea adiacendete a `<tag1>` agregale estos estilos
- combinador `<tag1> ~ <tag2>` indica a todo `<tag2>` que tenga como hermano general a `<tag1>` agregale estos estilos
- combinador `<tag1> > <tag2>` indica la etiqueta `<tag2>` que sea hijo directo de la etiqueta `<tag1>` agregale estos estilos

- combinador `<tag1> <tag2>` indica a todo `<tag2>` que este dentro de `<tag1>` agregale estos estilos

## Display

- Block: indica que el elemento ocupara el 100% de espacio que tenga sin importar que tanto este ocupando el contenido
- Inline: el display inline indica que el elemento solo ocupara el espacion que que reuiera su contenido su contenido, muy importante es que no se le puede asignar margenes de arriba y abajo y nose le puede dar el width
- inline-block: 
