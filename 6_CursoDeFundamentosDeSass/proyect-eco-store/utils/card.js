class cardData {
  constructor(id, img, description, title) {
    this.id = id;
    this.img = img;
    this.description = description;
    this.title = title;
    this.btnContent = `<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M3.34255 7.7779C3.5687 7.23194 3.90017 6.73586 4.31804 6.31799C4.7359 5.90012 5.23198 5.56865 5.77795 5.3425C6.32392 5.11635 6.90909 4.99995 7.50004 4.99995C8.09099 4.99995 8.67616 5.11635 9.22213 5.3425C9.7681 5.56865 10.2642 5.90012 10.682 6.31799L12 7.63599L13.318 6.31799C14.162 5.47407 15.3066 4.99997 16.5 4.99997C17.6935 4.99997 18.8381 5.47407 19.682 6.31799C20.526 7.16191 21.0001 8.30651 21.0001 9.49999C21.0001 10.6935 20.526 11.8381 19.682 12.682L12 20.364L4.31804 12.682C3.90017 12.2641 3.5687 11.7681 3.34255 11.2221C3.1164 10.6761 3 10.0909 3 9.49999C3 8.90904 3.1164 8.32387 3.34255 7.7779Z" stroke="#3F3F46" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>`;
  }
  getComponett() {
    return `
    <div class="skincare__card">
        <div class="card__svg">
          ${this.btnContent}
        </div>
        <div class="card__img">
          <img src="${this.img}" alt="${this.title}" >
        </div>
        <div class="card__content">
            <h3 class="card__title">${this.title}</h3>
            <p class="card__description">${this.description}</p> 
        </div>
    </div>`;
  }
}

const createData = (id, img, title, body) => {
  return {
    id,
    img,
    title,
    body,
  };
};

const URL_Products = "./assets/products/";
const productSkincare = [
  createData(
    1,
    "./assets/img/products/guasha-kit.jpg",
    "Cepillo de bambú",
    "Paquete de 2 unidades hecho 100% de materiales ecológicos."
  ),
  createData(
    2,
    `./assets/img/products/hand-cream.jpeg`,
    "Crema para manos",
    "Crema hidratante con eucalipto ayuda al cuidado de la piel."
  ),
  createData(
    3,
    `./assets/img/products/serum.jpg`,
    "Serum hidratante",
    "Sérum hecho a base de extractos naturales y ácido hialurónico."
  ),
  createData(
    4,
    `./assets/img/products/organic-kit.jpg`,
    "Kit de viaje",
    "Incluye peine y cepillo de bambú, jabón orgánico de eucalipto y shampoo tamaño de viaje."
  ),
  createData(
    5,
    `./assets/img/products/organic-soap.jpg`,
    "Set de jabones ",
    "Paquete de 2 unidades jabón de carbón activado con coco."
  ),
  createData(
    6,
    `./assets/img/products/guasha-kit.jpg`,
    "Kit de skincare",
    "Incluye 2 rodillos y una gua sha."
  ),
];

function iterable(products) {
  return products.map((element) => {
    const card = new cardData(
      element.id,
      element.img,
      element.body,
      element.title
    );
    return card;
  });
}

const cardElementsSkincare = iterable(productSkincare);

cardElementsSkincare.map((element) => {
  const skincare = document.querySelector(".skincare__Product");
  // const container = document.createElement("div");
  // container.classList.add("skincare__container");
  // const card = document.createElement("div");
  skincare.innerHTML += element.getComponett();
  // skincare.appendChild(container);
});

const productFurniture = [
  createData(
    1,
    "./assets/img/furniture/wood-chair.jpg",
    "Silla de bambú",
    "Disponible en 2 colores: blanco y negro, acabado de bambú."
  ),
  createData(
    2,
    "./assets/img/furniture/small-table.jpeg",
    "Banco pequeño",
    "Ideal para agregar un toque de estilo a tus espacios."
  ),
  createData(
    3,
    "./assets/img/furniture/stand-table.jpg",
    "Buró",
    "Elaborado con bambú, cuenta con 2 compartimentos para guardar cosas . "
  ),

  createData(
    4,
    "./assets/img/furniture/mueble.jpg",
    "Cajonera",
    "Paquete de 2 unidades hecho 100% de materiales ecológicos."
  ),
  createData(
    5,
    "./assets/img/furniture/mirror.jpg",
    "Espejo de pared",
    "Paquete de 2 unidades hecho 100% de materiales ecológicos."
  ),
  createData(
    6,
    "./assets/img/furniture/desk-lamp.jpg",
    "Lámpara bambú",
    "Paquete de 2 unidades hecho 100% de materiales ecológicos."
  ),
  createData(
    7,
    "./assets/img/furniture/stand.jpg",
    "Mesita decorativa",
    "Paquete de 2 unidades hecho 100% de materiales ecológicos."
  ),
  createData(
    8,
    "./assets/img/furniture/mini-table.jpg",
    "Mesa de desayuno",
    "Paquete de 2 unidades hecho 100% de materiales ecológicos."
  ),
  createData(
    9,
    "./assets/img/furniture/table.jpg",
    "Mesa decorativa",
    "Paquete de 2 unidades hecho 100% de materiales ecológicos."
  ),
];

const cardElementsFurniture = iterable(productFurniture);
console.log(cardElementsFurniture);
cardElementsFurniture.map((element) => {
  const furniture = document.querySelector(".furniture__Product");
  furniture.innerHTML += element.getComponett();
  // furniture.appendChild(skincare);
});
