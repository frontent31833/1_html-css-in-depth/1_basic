# SASS

## Coniciendo SASS

### ¿Qué es SASS y en qué se diferencia de otros preprocesadores?

- `SASS` con sus suglas del ingles Syntactically Awesome Style Sheets, es un preprocesador de css que le da mas funcionalidades adicionales a css como ser declaracion de variables, selectores anidados y mas.
- este preprocesador esta basado en `RUBY`
- Caracteristicas de SASS:

  - Variables: las variables nos serviran para guardan un dato por ejemplo un color
  - Mixins: Los mixins son parecidas a las funciones ya que reciben parametros de entrada
  - Modularizacion: haciendo uso de la palbara `use` podremos modularizar nuestro codigo
  - Selectores anidados: enidar selectores nos permitira escribir menos codigo
  - Herencia:

- Diferencias entre `.sass` y `.scss`

  - `.sass` utilioza la sibtaxis identada quitando lo tradicionales `{}`
  - `.scss` utiliza la sintaxis tradicional

  ![Alt text](image.png)

- Lecturas Recomendadas:
  - [REpositorio del proyecto](https://github.com/platzi/cursos-sass)
  - [Proyecto del curso](https://anamdiazs.github.io/eco-store-platzi)
  - [Documentacion SASS](https://sass-lang.com/documentation/)
  - [Curso de figma basico: Prototipado e interfaces](https://platzi.com/cursos/figma-basico/)
  - [Figma Proyecto](<https://www.figma.com/file/Em1aDiIHmqozHpUAjsYhT7/Eco-Store-Mockups-(Copy)?node-id=0-1&t=xqtULKy5UPfvz8kx-0>)
  - [Curso de Fundamentos de Sass: Crea tu Primera Landing Page.pdf - Google Drive](https://drive.google.com/file/d/1MQLGLYXPD-JFI4ad_5HmQpox2xMB4hHk/view?usp=share_link)

### ¿Como funcionan los preprocesadores?

- Un preprocesador es una herramienta que nos permite escribir `pseudocodigo` reciviendo como parametro los estilos que seran convertidos a css, su funcionamiento es similar a los traductores.

- Los estilos css son leidos por el navegador, es el encargado de leer linea por linea el codigo css y aplicarlo a los elelementos HTML, de esta manera lograr que todas las etiquetas html esten estilizadas y proporcionar una experiemcia atractiva al usuraio

![https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%20de%202023-04-12%2009-27-13-82db628c-51ed-4b6a-a515-0084be1833ac.jpg](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%20de%202023-04-12%2009-27-13-82db628c-51ed-4b6a-a515-0084be1833ac.jpg)

#### Ventajas de utilizar un Preprocesador:

las ventajas de usar un preprocesador son la rapidez y la productividad, permitiendo escribir codigo css de manera mucho mas rapida y sencilla ahoorando mucho tiempo al desarrollador. Tambien hacen que el mantenimiento del codigo sea mucho mas facil, y adicional a ello permite que sea mas facil trabajar con paginas web reponsivas.

#### Tipos de Preprocesadores:

- [Stylus](https://stylus-lang.com/) y [less](https://lesscss.org/) son preprocesadores que estan basados en js y que se procesan desde el lado del cliente, a diferencia de Sass que esta basado en Ruby que se procesa desde el lado del servidor.
- El Less los nombres de las variables se inicializan con un `@` y en sass con un `$`

## Instalación y configuración del proyecto

### Anatomía de un proyecto de SASS e instalación y configuración del entorno de trabajo

#### Proceso de compilado

![Ejemplo estructura del proyecto](image-1.png)

- Se debe tomar en cuenta tres puntos muy importnates:
  - Input File: Es el archivo en el cual se escriben todos los estilos con la sintaxis de sass es iportante que el archivo tenga la extencion de `.scss`
  - Output Fil: Es el archivo de salida en el cual estaran nuestros estilos con la sintaxis de css provenientes del archivo de entrada, no se debe modificar
  - Comandos para ejecutar y compilar Sass:
    - Tipos de instalacion de SASS:
      - Intalacion global
      - Usando node js
      - Dart Sass
      - La API de Javascript

## REglas y Uso

### Estructura de la hoja de estilos y variables

- ![Alt text](image-2.png)
- ![Alt text](image-3.png)
- ![Alt text](image-4.png)

#### Tipos de variables

- Variable: Es un espacio asignado en memoria que guarda solo un valor, pueden ser declarados en cualquier parte del la hoja de estilos
- Su declaracion se hace con `$`

```
$nombre-variable: contenido;
```

![Alt text](image-5.png)

- `Declarativo` se basa en el `que` se debe hacer
- `Imperativo` se basa en el `como` se debe hacer

- !default

![Alt text](image-6.png)

### Uso de selectores, scope de las variables y shadowing

- Un selector define sobre que elementos se aplicaran un conjunto de reglas css, existen selectores de clase, id, atributo, tipo

- El scope hace referencia hacia el `contexto` donde existe la variable, existem 2 tipos:
  - Locales: Estan declradas dentro de un bloque y este solo existe dentro de ese bloque y tambien para los selectores que estan dentro de ese bloque
  - globales: esta variables tienen un scope global, se les puede acceder desde cualquier lugar del codigo
  - shadowing: Las variables globales y locales pueden tener el mismo nombre siempre y cuando esten un escope diferente
  - !global flag: En caso de querer cambiar el valor global de una variable dentro del scope local se utilioza `!global`

## At Rules: CSS y nesting

- Nos ayudan a modularizar codigo
- At Rules CSS: Las at rules es una declracion que cuimple diferentes funciones, si usa con el `@`, y tiene su sitaxis propia. Las at rules nos ayudan a mantener compatibiliodad con las verciones de SASS

- Ejemplo de at-rules:

![Alt text](image-7.png)

- `@use` nos permite importar estilos y funciones desde otros archivos, es casi similar a la egla `@import`

- `@import` se encarga de hacer los estilos globales
- `@function` nos permite crar funciones personalizadas
- `@forward`: esta recive como parametro una url y no permitira cargar los estilos de nuestra hoja, es muy importatnte usar `use` para que nuestros modulos existan dentro de nuestro archivo `scss`
- `@extend`: nos permite hacer la herencia
- `@at-root`: se encarga de cargar nuestros estilos dentro de la root de nuestros estilos

- `@include`: nos ayuda a invocar los mixins.
- `@error, @warn @debug`: sirver para cuando hay un error, una advertencia o se quiere debugear, respectivamente
- `@for, @if, @each, @while`: tienen que ver con estructuras de control, se pueden usar dentro de una función

### Nesting

- El nesting o anidacion de selectores, nos permite anidar selectores para un desarrollo mas rapido y poder estilarlo segun bayan apareciendo.

## Expresiones: Literales y Operaciones

- una exprecion es todod aquello que va de lado derecho de una variable
- Son mucho mas poderosas que los valores simples de css, ya que se pasan como argumentos a mixins y funciones
- Ejemplos:

  - Numeros
  - Strings
  - Colores
  - Booleans
  - Null
  - Listas
  - Mapas

  ## ¿Qué es un mixin en CSS?

  - PErmiten definir estilos que lo tendremos diponobles en cualquier parte de la hoja de estilos

  ![Alt text](image-8.png)

- algunos usos que se le puden dar a los `mixings`
  Los mixins en Sass son útiles para reutilizar estilos en varios lugares de tu hoja de estilo. Son bloques de código reutilizables que pueden contener propiedades CSS, declaraciones y hasta reglas completas. Al usar mixins, puedes:

1. **Evitar duplicación de código:** Si tienes estilos que se repiten en varios lugares de tu hoja de estilo, en lugar de copiar y pegar, puedes definir un mixin y reutilizarlo en múltiples ubicaciones.

2. **Mejorar la legibilidad y mantenibilidad:** Los mixins permiten encapsular estilos específicos en un solo lugar. Esto hace que tu código sea más limpio y más fácil de entender. Si necesitas hacer cambios en los estilos, solo tienes que hacerlo en un solo lugar, lo que reduce la posibilidad de errores.

3. **Modificar estilos en un solo lugar:** Si tienes que hacer ajustes en un estilo compartido en varios lugares, solo necesitas hacerlo en el mixin, y los cambios se aplicarán automáticamente en todas las instancias donde se esté utilizando.

4. **Crear estilos más flexibles:** Los mixins pueden aceptar argumentos, lo que significa que puedes personalizar cómo se aplican los estilos en diferentes contextos. Esto te da más flexibilidad para adaptar los estilos a diferentes situaciones.

Por ejemplo, imagina que tienes estilos para botones que se repiten en varias partes de tu sitio web. En lugar de escribir los mismos estilos una y otra vez, puedes definir un mixin de botón con todos los estilos necesarios y luego reutilizarlo en cada botón.

```scss
@mixin boton {
  padding: 10px 20px;
  font-size: 14px;
  background-color: #007bff;
  color: white;
  border: none;
  border-radius: 5px;
  cursor: pointer;
}

.boton-primario {
  @include boton;
}

.boton-secundario {
  @include boton;
  background-color: #6c757d;
}
```

En este ejemplo, el mixin `boton` contiene los estilos comunes de un botón. Luego, los estilos específicos de los botones primarios y secundarios se crean utilizando este mixin, pero con posibilidad de personalización adicional.

Los mixins en Sass son una herramienta poderosa para crear hojas de estilo más eficientes y mantenibles, ya que te permiten abstraer y reutilizar estilos de manera efectiva.
Cabe recalcar que los mixins pueden recivir parametros, los parametros deben iniciar por `$`

## Creación de nuestras propias funciones

- las funciones no pemriten definir operaciones mas avanzadas y complejas, se definen usnado la regla `@function`

![Alt text](image-9.png)

- Operadores:

![Alt text](image-10.png) ![Alt text](image-11.png) ![Alt text](image-12.png)

## Instrucciones para instalar y configurar Sass mediante Node.js

1. Abre una terminal y navega hasta la carpeta raíz de tu proyecto.
   Asegúrate de tener Node.js instalado en tu sistema. Puedes verificarlo escribiendo node -v en la terminal. Si no lo tienes instalado, ve al [sitio web oficial](https://nodejs.org/en) de Node.js para descargarlo e instalarlo.

2. Ejecuta el siguiente comando para instalar Sass a nivel global:

```
npm install -g sass
```

3. Ahora que tienes Sass instalado a nivel global, puedes compilar tus archivos Sass en CSS con el siguiente comando en la terminal:

```
sass input.scss output.css
```

4. Reemplaza “input.scss” con la ruta y el nombre de tu archivo Sass, y “output.css” con la ruta y el nombre de tu archivo CSS de salida. Por ejemplo:

```
sass styles/main.scss styles/main.css
```

5. Si quieres compilar automáticamente tus archivos Sass en CSS cada vez que hagas cambios, puedes usar la opción --watch:

```
sass --watch input.scss:output.css
```

6. Si estás utilizando Node.js en tu proyecto, también puedes usar un paquete de npm llamado sass para compilar tus archivos Sass en CSS. Para instalarlo, ejecuta el siguiente comando:
```
npm install sass
```
7. En tu archivo de configuración de Node.js (como package.json) agrega un script para compilar tus archivos Sass en CSS. Por ejemplo:
```
"scripts": {
  "build:css": "sass input.scss output.css"
}
```
8. Ahora puedes ejecutar el script con el siguiente comando:
```
npm run build:css
```

