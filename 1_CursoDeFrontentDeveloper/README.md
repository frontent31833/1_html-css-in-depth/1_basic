# **Frontend Developer**

## ¿Qué es HTML y CSS?

- **HTML (HyperText Markup Language)** es un lenguaje de marcado que se utiliza para estructurar y presentar contenido en la web mediante etiquetas que definen cómo se debe mostrar el contenido, como texto, imágenes, videos, audios, etc.

- **CSS (Cascading Style Sheets)** es una hoja de estilos en cascada que se utiliza para dar estilo a las etiquetas presentes en documentos HTML.

![Imagen](./img/image.png)

## Motores de Renderizado:

Los motores de renderizado convierten los archivos HTML y CSS en píxeles en la pantalla. Estos motores se encuentran en los navegadores.

Navegadores y sus motores:

| Navegador | Motor    |
| --------- | -------- |
| Chrome    | Blink    |
| Edge      | EdgeHTML |
| Safari    | WebKit   |
| Firefox   | Gecko    |

Los motores realizan cinco pasos para compilar el código y renderizarlo en pantalla:

![Conversion](./img/Conversion.png)

1. Transformación a objetos: Representación en un árbol de objetos conocido como el DOM (Document Object Model). Cada nodo es una etiqueta del HTML, con sus respectivos estilos en el CSSDOM (Cascade Style Sheet Object Model).

2. Cálculo de estilos: Cálculo de estilos correspondientes para cada nodo del DOM relacionado con el CSSDOM.

3. Cálculo de dimensiones: Cálculo de las dimensiones y la posición de cada nodo.

4. Renderizado: Representación de elementos como cajas o contenedores.

5. Composición y pintado: Agrupación de cajas en capas para convertirlas en una imagen que se muestra en pantalla.

## Anatomía de un Documento HTML y sus Elementos

![Etiquetas HTML](./img/ETIQUTAhTML.png)

- Elementos HTML: Cada parte que compone un archivo HTML. Estructura que contiene:
  - Etiquetas: Representación de un elemento HTML. Etiquetas de apertura `<etiqueta>` y cierre `</etiqueta>`.
  - Contenido: Texto o elementos encerrados por la etiqueta. Opcional en algunas.

![Atributos HTML](./img/atributoHtml.png)

- Atributos HTML: Propiedades en la etiqueta de apertura que manejan el comportamiento del elemento.

![Anidamiento](./img/Anidamiento.png)

- Anidamiento de elementos: Envolver etiquetas en otras. Cada elemento HTML es una caja que contiene otros elementos o cajas.

![Elementos Vacíos](./img/Vacios.png)

- Elementos Vacíos: Representados solo en etiquetas de apertura, como la etiqueta de imagen `<img...>`.

Estructura básica de un documento HTML:

![Estructura Básica HTML](./img/StructuraBasicaHtml.png)

1. Etiqueta `<!DOCTYPE html>`: Versión 5 de HTML.
2. Etiqueta `<html>`: Elemento raíz del documento. Contiene todo el contenido.
3. Etiqueta `<head>`: Metainformación. Enlaces a archivos CSS y JavaScript, título, imagen de la pestaña del navegador.
4. Etiqueta `<body>`: Contenido de la página. Padre de todas las etiquetas HTML, excepto las de metainformación.

Comentarios de HTML: `<!-- Comentario -->`

## HTML Semántico

- HTML semántico: Cada elemento tiene su etiqueta que lo define correctamente. Evita etiquetas genéricas como `<div>` o `<span>`.

- Problema con `<div>`: Define un bloque genérico, sin valor semántico. Utilizado para diseño como contenedores.

- Etiquetas semánticas:

  - `<header>`: Encabezado de la página.
  - `<nav>`: Menú de navegación.
  - `<section>`: Sección de la página.
  - `<footer>`: Pie de página o de sección.
  - `<article>`: Artículo con encabezado, navegación, contenido y pie propios.

  Evita el uso excesivo de `<div>`.

![HTML Semántico](./img/sematicHtml.png)

Ventajas de HTML semántico:

- Accesibilidad.
- Mejor posicionamiento (SEO).
- Código legible y mantenible.
- Ayuda a buscadores como Google.

## Maquetación con CSS

![Imagen](./img/image-2.png)

## Anatomía de una Declaración CSS: Selectores, Propiedades y Valores

![Imagen](./img/image-1.png)

## Tipos de Selectores: Básicos y Combinadores

### Básicos

![Imagen](./img/image-3.png)

1. Selector de tipo: `<div>`.
2. Selector de clase: `.clase`.
3. Selector de ID: `#id`.
4. Selector de atributo: `[atributo=valor]`.
5. Selector universal: `*`.

### Combinadores

![Imagen](./img/image-4.png)

1. Descendientes: `div p`.
2. Hijo directo: `div > p`.
3. Elemento adyacente: `div + p`.

### Pseudoclases y Pseudoelementos

![Imagen](./img/image-5.png)

#### Pseudoclases

Selector: `selector:pseudoclase`.

#### Pseudoelementos

Selector: `selector::pseudoelemento`.

## Cascada y Especificidad en CSS

#### Qué es la cascada en CSS

la cascada en css es el concepto que determina que estilos se colocan sobre otros, priorizando a aquellos que se encuentran mas abajo del codigo
![Alt text](./img/image-6.png)

#### Qué es especificidad en CSS

La especificidad consiste en dar un valor a una regla CSS sobre qué tan específico es el estilo, esto para que los navegadores puedan saber qué estilos aplicar sobre otros, independientemente de dónde se encuentren en el código. El estilo se aplicará donde la especificidad sea mayor.

##### Tipos de especificidad en CSS

Existen 6 tipos de especificidad con su respectivo valor, donde X es la cantidad de estilos que lo contienen. Mira la siguiente imagen:
![Alt text](./img/image-7.png)

- Valor con mayor especificidad: La palabra reservada `!important` asigna a la propiedad una especifidad de 10000, pero es cosiderado una mala practica

- Estilos en linea:
  son las propiedades css que se escribem en la etiqueta con la propiedad `style`

### **Tipos de display**

![Alt text](./img/image-8.png)

[guia completa](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

### **Modelo de caja:**

El modelo de caja se compone de cuatro elementos: margin, border, padding y contenido.
![Alt text](./img/image-9.png)

### **Colapso de márgenes**

El colapso de márgenes sucede cuando dos elementos bloque adyacentes tienen un determinado valor de margin, entonces estos márgenes se solapan en un solo valor, el mayor de ambos.
![Alt text](./img/image-10.png)

### **Posicionamiento en CSS**

El posicionamiento en CSS consiste en cómo un elemento se situará, con respecto a su elemento padre y al flujo normal del documento. El flujo normal del documento es el orden de los elementos establecidos en el HTML.
![Alt text](./img/image-11.png)

1. `Static`: este tipo de posicionamiento es el que llevan por defecto todas la etiquteas html, respeta el flujo normal de la pagina donde las propiedades de posicion no pueden ser establecidas.
2. `Relative`: respeta el flujo normal de la pagina donde las propiedades de posicion si pueden ser establecidas.
3. `absolute`: consiste en quitar al elemento del flujo normal de la pagina, donde las propiedades de posicionamiento si pueder ser establecidas
4. `fixed` (fija): este tipo de posicionamiento consiste ne fijar un elemento en una seccon de la pagina
5. `sticky` (variable fija): fija al elelemento mientras su cointenedor sea visible

### **Z-index y el contexto de apilamiento**

El de apilamiento es la superposicion de capas o elementos a lo largo del eje z del navegador
![https://media1.giphy.com/media/1IvbqeWg7gLlRi2TAC/giphy.gif?cid=790b7611ba7156183cec0ef1ba9e76527d6adfbc2610e94c&rid=giphy.gif&ct=g](https://media1.giphy.com/media/1IvbqeWg7gLlRi2TAC/giphy.gif?cid=790b7611ba7156183cec0ef1ba9e76527d6adfbc2610e94c&rid=giphy.gif&ct=g)

<!-- ![](https://media.giphy.com/media/Hp1EYtepZbFJledhsZ/giphy.gif) -->

planos y ejes

![https://static.platzi.com/media/articlases/Images/animationland03.PNG](https://static.platzi.com/media/articlases/Images/animationland03.PNG)

la propiedad `z-index`

![https://static.platzi.com/media/articlases/Images/frontend_developer27.png](https://static.platzi.com/media/articlases/Images/frontend_developer27.png)

### Propiedades y valores de CSS más usados

![https://static.platzi.com/media/articlases/Images/frontend_developer28.png](./img/image-12.png)

[documentacion](https://cssreference.io/)

## Diseño Responsivo

-

### Unidades de medida:

Las unidades de medida establecen una longitud para un determinado elemento o tipografía. Existen dos tipos de medidas: absolutas y relativas.

![https://static.platzi.com/media/articlases/Images/frontend_developer29.png](./img/image-13.png)

### Responsive Design

El diseño responsivo (Responsive Design) consiste en un conjunto de herramientas para que tu sitio se vea bien en varias medidas de pantalla, esto incluye imágenes, tipografía, internacionalización de la página y entre otros.

Para lograr el diseño responsivo podemos usar las `media queries`

- Las media queries son reglas de css que nos ayudan a cambiar el comortamiento de la pagina como ser ca,biar el color de algun texto el tamaño, cambiar el estilo en un cierto rango de pantalla

- Estructura de la media querie
  La estructura de una media querie consiste en empezar con @media, seguido del tipo de la media querie estableciendo un rango, envolviendo las reglas CSS dentro de ese rango.

![https://static.platzi.com/media/articlases/Images/frontend_developer31.png](./img/image-14.png)

## Arquitectura en CSS

### ¿Qué son las arquitecturas CSS?

- nos permite manejar el codigo css con una serie de reglas y patrones para facilitar su lectura, refactorizacion y escalabilidad

![https://static.platzi.com/media/articlases/Images/frontend_developer33.png](./img/image-15.png)

### OOCSS, BEM, SMACSS, ITCSS y Atomic Design

OOCSS: La arquitectura `OOCSS` (Object Oriented CSS) consiste en separar la estructura principal y la piel o máscara. En otras palabras, consiste en tener `objetos` que son estructuras principales. Estos `objetos` estarán unidos en una `máscara`, donde esta será la que cambie pero manteniendo la estructura intacta.

![https://static.platzi.com/media/articlases/Images/frontend_developer34.png](./img/image-16.png)

BEM: `bloque`, `elemento` y `modificador`. La arquitectura BEM (Block-Element-Modifier) es una de las más utilizadas actualmente. Consiste en `manejar` los elementos en clases definidas por `bloques`, `elementos` y `modificadores`.

- Bloque: es la estructura principal que es `contenedora` de varios elementos.
- Elemento: es el elemento HTML que hace referencia el contenedor.
- Modificador: es un estilo específico para el elemento. Por ejemplo, un botón que tenga un color diferente a los demás, esto tiene relación con la especificidad.

![https://static.platzi.com/media/articlases/Images/frontend_developer35.png](./img/image-17.png)

SMACSS: La arquitectura SMACSS (Scalable and Modular Architecture for CSS) indica el orden de componentes que estarán ubicados en carpetas. La unión de estos componentes dará como resultado tu página web con estilos.

- Base: elementos base, como botones, títulos, enlaces.
- Layout: estructura de la página, relacionado con el Responsive Design.
- Módulos: elementos que contienen a los elementos base.
- Estado: estilos relacionados con el comportamiento de elemento, relacionado con las pseudoclases y pseudoelementos.
- Temas: conjunto de estilos que definen tu página web.

![https://static.platzi.com/media/articlases/Images/frontend_developer36.png](./img/image-18.png)

ITCSS: La arquitectura ITCSS (Inverted Triangle CSS) consiste en `separar` los `archivos` del `proyecto`; mediante ajustes, herramientas, elementos, entre otros. Todo esto para manejar los detalles de especificidad, claridad y magnitud.

![https://static.platzi.com/media/articlases/Images/frontend_developer37.png](./img/image-19.png)


Atomic Design: La arquitectura Atomic Design también es una de las más utilizadas actualmente. Consiste en manejar los elementos como una estructura mínima, a partir de la unión de varias de estas, dará como resultado los estilos de la página web. Se basa en la estructura mínima de la materia, los átomos.

- Átomos: estructura mínima; como botones, enlaces, títulos, entre otros.
- Moléculas: unión de átomos.
- Organismos: unión de moléculas.
- Plantillas: unión de organismos.
- Páginas: unión de plantillas.

![https://static.platzi.com/media/articlases/Images/frontend_developer38.png](./img/image-20.png)