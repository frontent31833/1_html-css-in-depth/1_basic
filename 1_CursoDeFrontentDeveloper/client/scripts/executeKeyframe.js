const elements = document.querySelectorAll(".card-item");

elements.forEach((element) => {
    
  window.setTimeout(() => {
    element.style.setProperty("--opacity", 1);
  }, 2500);

  element.animate(
    [
      { transform: "scale(0)", opacity: 0.5 },
      { transform: "scale(1)", opacity: 1 },
    ],
    {
      duration: 2500,
      delay: 1400,
      easing: "ease",
    }
  );
});
